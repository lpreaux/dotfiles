-- Line number
vim.opt.number = true
vim.opt.relativenumber = true

-- Indent
vim.opt.tabstop = 4 
vim.opt.softtabstop = 4
vim.opt.shiftwidth = 4
vim.opt.expandtab = true
vim.opt.smartindent = true

-- Files
vim.opt.swapfile = false
vim.opt.backup = false
vim.opt.undodir = os.getenv("HOME") .. "/.vim/undodir"
vim.opt.undofile = true

-- Search mode
vim.opt.hlsearch = false
vim.opt.incsearch = true

-- auto-scroll
vim.opt.scrolloff = 8

-- Sign column
vim.opt.signcolumn = "yes"

-- Vim update time
vim.opt.updatetime = 50

-- Color column 80
vim.opt.colorcolumn = "80"

