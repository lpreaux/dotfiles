eval "$(/usr/local/bin/brew shellenv)"

VIM="nvim"

export PATH="$HOME/.local/scripts:$PATH"
export PATH="$VOLTA_HOME/bin:$PATH"


alias vim="$VIM"


# Added by OrbStack: command-line tools and integration
# This won't be added again if you remove it.
source ~/.orbstack/shell/init.zsh 2>/dev/null || :
